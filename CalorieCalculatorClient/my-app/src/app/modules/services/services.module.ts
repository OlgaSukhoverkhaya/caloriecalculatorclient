import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DataService } from '../../services/data.service';
import { MealDataService } from '../../services/mealdata.service';
import { AuthenticationService } from '../../services/autentication.service';
import { UserService } from '../../services/user.service';
import { AuthGuard } from '../../components/forms/authentication/auth.guard';
import { RegistrationService } from '../../services/registration.service';
@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [],
  providers: [DataService, MealDataService, AuthenticationService, UserService, AuthGuard, RegistrationService]
})
export class ServicesModule { }
