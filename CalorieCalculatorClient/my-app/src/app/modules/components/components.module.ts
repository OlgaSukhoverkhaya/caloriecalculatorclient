import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from "@angular/platform-browser";
import {ProductListComponent,AddMealDialog} from '../../components/forms/productlist/productlist.component';
import {ServicesModule} from '../services/services.module';
import {DemoMaterialModule} from './demomaterials.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatNativeDateModule} from '@angular/material';
import {AddproductComponent} from '../../components/forms/addproduct/addproduct.component';
import {MymealsbydateComponent} from '../../components/forms/mymealsbydate/mymealsbydate.component';
import {AuthenticationComponent} from '../../components/forms/authentication/authentication.component';
import {RegistrationComponent} from '../../components/forms/registration/registration.component';
import { Routes, RouterModule } from '@angular/router';
import { VipprogressComponent } from '../../components/forms/vipprogress/vipprogress.component';
import { AddvipuserdataComponent } from '../../components/forms/addvipuserdata/addvipuserdata.component';

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    ServicesModule,
    DemoMaterialModule,
    FormsModule,
    MatNativeDateModule,
    ReactiveFormsModule,
    RouterModule
  ],
  declarations: [
    ProductListComponent,
    AddproductComponent,
    AddMealDialog,
    MymealsbydateComponent,
    AuthenticationComponent,
    RegistrationComponent,
    VipprogressComponent,
    AddvipuserdataComponent
  ],
  exports:[
    ServicesModule,
    ProductListComponent,
    DemoMaterialModule,
    AddproductComponent,
    AddMealDialog,
    MymealsbydateComponent,
    AuthenticationComponent,
    RegistrationComponent,
    VipprogressComponent,
    AddvipuserdataComponent],
    entryComponents:[AddMealDialog]
})
export class ComponentsModule { }
