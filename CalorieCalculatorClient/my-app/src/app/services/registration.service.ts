import {User} from '../domain/user';
import { Injectable } from "@angular/core";
import { HttpHeaders, HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs/Observable';
import { UserRole } from '../domain/userrole';

export const REST_URL_REGISTRATION = 'http://localhost:49246/api/register';

@Injectable()
export class RegistrationService {

    Options = { headers: new HttpHeaders({ 'Content-Type': 'application/json'})};

    constructor(private http: HttpClient) { }
    
    createUser(user: User): Observable<User> {
        let body = JSON.stringify(user);
        return this.http.post<User>(REST_URL_REGISTRATION+"/user", body, this.Options);
    }

    addUserRole(userRole:UserRole) :Observable<UserRole>{
        let body = JSON.stringify(userRole);
        return this.http.post<UserRole>(REST_URL_REGISTRATION+"/role", body, this.Options);
    }
}