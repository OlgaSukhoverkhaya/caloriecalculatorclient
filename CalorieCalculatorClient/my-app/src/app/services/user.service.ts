import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { User } from '../domain/user';
import { Observable } from "rxjs/Observable";
import { AuthenticationService } from './autentication.service';
import { VipUser } from '../domain/vipuser';

export const REST_URL_USER = 'http://localhost:49246/api/users';
export const REST_URL_VIP_USER = 'http://localhost:49246/api/vipusers';
export const REST_URL_ROLE = 'http://localhost:49246/api/roles/userid';

@Injectable()
export class UserService {

    Options = { headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + this.authenticationService.token }) };

    constructor(
        private http: HttpClient,private authenticationService: AuthenticationService) { }

    getUsers(): Observable<User[]> {
        
        return this.http.get<User[]>(REST_URL_USER, this.Options);
    }

    getUserByUsername(username: string): Observable<User> {
    
        return this.http.get<User>(REST_URL_USER + "/" + username, this.Options);
    }

    getUserByUserId(id:number): Observable<User> {
    
        return this.http.get<User>(REST_URL_USER + "/userid/" + id, this.Options);
    }

    getRoleByUserId(id: number): Observable<String[]> {
        return this.http.get<String[]>(REST_URL_ROLE + "/" + id, this.Options);
    }

    getVipUserByUserId(id: number): Observable<VipUser> {
        return this.http.get<VipUser>(REST_URL_VIP_USER + "/userid/" + id, this.Options);
    }

    UpdateVipUserData(vipuser:VipUser): Observable<VipUser> {
        let body = JSON.stringify(vipuser);
        return this.http.put<VipUser>(REST_URL_VIP_USER+"/update/"+vipuser.vipUserId, body, this.Options);
    }
    AddVipUser(vipuser:VipUser)
    {
        let body = JSON.stringify(vipuser);
        return this.http.post<VipUser>(REST_URL_VIP_USER, body, this.Options);
    }
}