import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HttpResponse } from '@angular/common/http';
import {Response} from '@angular/http';
export const REST_URL_AUTH = 'http://localhost:49246/api/authenticate';
const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
@Injectable()
export class AuthenticationService {
    public token: any;
    error:any;  
    public username;
    constructor(private http: HttpClient) {
        // set token if saved in local storage
        var currentUser = JSON.parse(localStorage.getItem('currentUser'));
        if(currentUser){
        this.token = currentUser.response.access_token;
       // console.log(this.token);
        }
    }
        
    login(username: string, password: string): Observable<boolean> {
        return this.http.post(REST_URL_AUTH, JSON.stringify({ username: username, password: password }),httpOptions)
            .map((response: any) => {
               if (response) { 
                   let r=response;
                     this.token =  r.access_token;
                     this.username=r.username;
                    // store username and jwt token in local storage to keep user logged in between page refreshes
                 localStorage.setItem('currentUser', JSON.stringify({response}));
                    // return true to indicate successful login
                    return true;
                }
                 else {
                    // return false to indicate failed login
                    return false;
            }});}

    logout(): void {
        // clear token remove user from local storage to log user out
        this.token = null;
        localStorage.removeItem('currentUser');
    }
}