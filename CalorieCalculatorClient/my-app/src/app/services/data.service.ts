import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Product } from '../domain/product';
import { Observable } from "rxjs/Observable";
import { AuthenticationService } from './autentication.service';

/*let httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json'});
};*/
export const REST_URL_PRODUCTS = 'http://localhost:49246/api/products';

@Injectable()
export class DataService {

    Options = { headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + this.authenticationService.token }) };

    constructor(private http: HttpClient,
        private authenticationService: AuthenticationService) {
    }

    getProducts(): Observable<Product[]> {
        return this.http.get<Product[]>(REST_URL_PRODUCTS, this.Options);
    }

    getProductByName(productName: string): Observable<Product> {
        return this.http.get<Product>(REST_URL_PRODUCTS + '/' + productName, this.Options);
    }

    getProductById(productId: string): Observable<Product> {
        return this.http.get<Product>(REST_URL_PRODUCTS + '/id/' + productId);
    }

    createProduct(product: Product): Observable<Product> {
        let httpOptions = this.Options;
        let body = JSON.stringify(product);
        return this.http.post<Product>(REST_URL_PRODUCTS, body, this.Options);
    }

    updateProduct(product: Product): Observable<Product> {
        let httpOptions = this.Options;
        let body = JSON.stringify(product);
        return this.http.put<Product>(REST_URL_PRODUCTS + '/' + product.productName, body, httpOptions);
    }

    deleteProduct(productName: string): Observable<Product> {
        return this.http.delete<Product>(REST_URL_PRODUCTS + '/' + productName);
    }
}