import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Meal } from '../domain/meal';
import { Observable } from "rxjs/Observable";
import { AuthenticationService } from './autentication.service';

export const REST_URL_MEALS = 'http://localhost:49246/api/meals';

@Injectable()
export class MealDataService {

    Options = { headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + this.authenticationService.token }) };

    constructor(private http: HttpClient,
        private authenticationService: AuthenticationService) {
    }

    getMealsByDate(date: string, userid: number): Observable<Meal[]> {

        return this.http.get<Meal[]>(REST_URL_MEALS + '/' + userid + '/' + date, this.Options);
    }

    createMeal(meal: Meal): Observable<Meal> {
        let Options = { headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + this.authenticationService.token }) };
        const httpOptions = Options;
        let body = JSON.stringify(meal);
        return this.http.post<Meal>(REST_URL_MEALS, body, httpOptions);
    }
    /*
        updateProduct(product: Product):Observable<Product> {
            let body=JSON.stringify(product);
            return this.http.put<Product>(REST_URL_PRODUCTS + '/' + product.productName, body,httpOptions);
        }
    
        deleteProduct(productName:string):Observable<Product> {
            return this.http.delete<Product>(REST_URL_PRODUCTS + '/' + productName);
        }*/
}