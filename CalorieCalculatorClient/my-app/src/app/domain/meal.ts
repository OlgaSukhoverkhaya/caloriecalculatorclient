import { Product } from './product';
import { User } from './user';
export class Meal {
    /*[Key]*/
    public mealId: number;
    public weight: number;
    public date: Date;
    public userId: number;
    public user: User;
    public productId: number;
    public product: Product;
}