import { Meal } from './meal';
    export class Product {
        /*[Key]*/
        public productId: number;
        public productName: string;
        public calorieCount: number;
        public fat: number;
        public protein: number;
        public carbo: number;
        public meals: Array<Meal>;
    }