
import { VipUser } from './vipuser';
export class Progress {
    /*[Key]*/
    public progressId: number;
    public initialweight: number;
    public currentweight: number;
    public calorietarget: number;
    public date:Date;
    public vipuserid:number;
}
