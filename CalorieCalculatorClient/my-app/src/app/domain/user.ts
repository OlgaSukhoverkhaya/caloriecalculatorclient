import { Meal } from './meal';
import { UserRole } from './userrole';
import { VipUser } from './vipuser';
export class User {
    /*[Key]*/
    public userId: number;
    public username: string;
    public password: string;
    public meals: Array<Meal>;
    public vipuser:VipUser;
    public userRoles:Array<UserRole>;
}