import { Progress } from "./progress";

export class VipUser {
    /*[Key]*/
    public vipUserId: number;
    public gender: string;
    public birthDate: string;
    public height: number;
    public userId:number;
    public progress:Array<Progress>;
}
