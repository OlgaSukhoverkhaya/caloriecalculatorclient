import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import {HttpModule} from '@angular/http';
import { AppComponent } from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ComponentsModule} from './modules/components/components.module';
import { StartpageComponent } from './components/pages/startpage/startpage.component';
import {RouterModule, Routes} from '@angular/router';
import { HomepageComponent } from './components/pages/homepage/homepage.component';

// определение маршрутов
const appRoutes: Routes =[
    { path: '', component: StartpageComponent},
    { path: 'home', component: HomepageComponent}
   // { path: '**', component: NotFoundComponent }
  ];

@NgModule({
    imports:
        [BrowserModule,
            FormsModule,
            HttpClientModule,
            HttpModule,
            BrowserAnimationsModule,
            ComponentsModule,
            RouterModule.forRoot(appRoutes)],
    declarations: [AppComponent,HomepageComponent,StartpageComponent],
    bootstrap: [AppComponent],
   
})
export class AppModule { }
