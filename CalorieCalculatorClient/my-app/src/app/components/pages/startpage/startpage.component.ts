import { Input } from '@angular/core';

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-startpage',
  templateUrl: './startpage.component.html',
  styleUrls: ['./startpage.component.css']
})
export class StartpageComponent implements OnInit {

  hasaccount: boolean = true;
  onChanged(hasaccount: any) {
    this.hasaccount = hasaccount;
  }
  onRegisterChanged(tologin: any) {
    this.hasaccount = tologin;
  }

  constructor() { }

  ngOnInit() {

  }

}
