import { UserService } from '../../../services/user.service';
import { User } from '../../../domain/user';
import { Component, OnInit, Output, EventEmitter, ViewChild, AfterViewInit } from '@angular/core';
import 'rxjs/add/operator/map'
import { MatTabChangeEvent } from '@angular/material';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit{

  public currentUser: User = new User();
  public onlineUser = JSON.parse(localStorage.getItem('currentUser'));
  public userid = this.onlineUser.response.user_id;
  public role: string;
  public isvip: boolean=false;

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.getLoggedUserRole();
    this.getLoggedUser();
  }

  static onTabchanged= new Subject();

  tabChanged(){
        HomepageComponent.onTabchanged.next();
    }

  getLoggedUser()
  {
    this.userService.getUserByUserId(this.userid)
    .subscribe(data=>this.currentUser=data);
  }

  getLoggedUserRole() {
          this.userService.getRoleByUserId(this.userid).subscribe(
            (res: string[]) => {
              if (res.indexOf("vipuser") > -1){
                this.role = "Vip account";
                this.isvip=true;
              }
              else {this.role = "Limited account"};
            }
          );
      }
}
