import { Component, OnInit, EventEmitter, Injectable, Output } from '@angular/core';
import {FormControl, FormGroupDirective, NgForm, Validators, FormGroup} from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../../services/autentication.service';


@Component({
  selector: 'app-authentication',
  templateUrl: './authentication.component.html',
  styleUrls: ['./authentication.component.css']
})

export class AuthenticationComponent implements OnInit {
  model:any={};
  error='';

  constructor(private router: Router,
    private authenticationService: AuthenticationService) { }

  ngOnInit() {
    this.authenticationService.logout();
  }

  login()
  {
    this.authenticationService.login(this.model.username,this.model.password)
    .subscribe(result=>
    {
      if(result==true)
      {
        this.router.navigate(['/home']); // login successful
      }
    },error => {this.error = 'Username or password is incorrect!';})
  }

  @Output() onChanged = new EventEmitter<boolean>();
    change(notregistered:any) {
        this.onChanged.emit(notregistered);
    }
  
}
