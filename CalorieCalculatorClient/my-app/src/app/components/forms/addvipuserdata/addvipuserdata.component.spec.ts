import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddvipuserdataComponent } from './addvipuserdata.component';

describe('AddvipuserdataComponent', () => {
  let component: AddvipuserdataComponent;
  let fixture: ComponentFixture<AddvipuserdataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddvipuserdataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddvipuserdataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
