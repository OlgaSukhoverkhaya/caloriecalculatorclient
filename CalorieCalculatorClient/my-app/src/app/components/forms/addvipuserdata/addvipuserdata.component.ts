
import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { UserService } from '../../../services/user.service';
import { VipUser } from '../../../domain/vipuser';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-addvipuserdata',
  templateUrl: './addvipuserdata.component.html',
  styleUrls: ['./addvipuserdata.component.css']
})
export class AddvipuserdataComponent implements OnInit {


  @Output() tableRefersh = new EventEmitter<boolean>();//
  selectedGender: string;
  day: string;
  month: string;
  year: string;
  message: string;
  height: number;

  gender = [
    'Male',
    'Female'
  ];
  public onlineUser = JSON.parse(localStorage.getItem('currentUser'));
  public userid = this.onlineUser.response.user_id;

  vipuser: VipUser;
  vid: number;
  constructor(private userService: UserService) { }

  ngOnInit() {
    this.getDataFromDatabase();
  }

  getDataFromDatabase() {
    this.userService.getVipUserByUserId(this.userid)
      .subscribe((data: VipUser) => {console.log(data)
        if (data!=null) {
          this.vipuser = data,
          this.day = this.vipuser.birthDate.substring(0, 2);
          this.month = this.vipuser.birthDate.substring(3, 5);
          this.year = this.vipuser.birthDate.substring(6, 10);
          this.selectedGender = this.vipuser.gender;
          this.height = this.vipuser.height;
          this.vid = this.vipuser.vipUserId;
        }
        else
        {
            this.AddVipUser();
        }
      }
      )
  }
 AddVipUser() {
        this.userService.getRoleByUserId(this.userid)
          .subscribe((result: string[]) => {
            if (result.indexOf("vipuser") > -1) {
              let vipuser:VipUser=new VipUser();
              vipuser.userId = this.userid;
              this.userService.AddVipUser(vipuser).subscribe(data => console.log(data));
            }
          }
          )
  }

  addOrUpdate(day: number, month: number, year: number, height: number, gender: string) {
    if (day > 31 || day < 1 || month > 12 || month < 1 || year > Number(new Date().getFullYear()) || year < 1900 || height > 250) {
      this.message = "Invalid data, controll you'r birthdate and/or height fields!";
    }
    else {
      this.userService.getVipUserByUserId(this.userid)
        .subscribe((data: VipUser) => {
          if (data) {
            let newVipUser: VipUser = new VipUser();
            newVipUser.birthDate = this.formatDate(day) + "." + this.formatDate(month) + "." + year.toString();
            newVipUser.vipUserId = data.vipUserId;console.log(newVipUser.vipUserId)
            newVipUser.height = height;
            newVipUser.gender = gender;
            newVipUser.userId = this.userid;
            this.userService.UpdateVipUserData(newVipUser)
              .subscribe(data => {
                this.tableRefersh.emit(true);
                this.message = "Your data is saved/updated!"
                return true;
              },
              error => {
                this.message = "Error saving/updating vipuserdata!";
                return Observable.throw(error);
              })
          }

        }

        )
    }
  }

  formatDate(int: number) {
    let field = int.toString();
    if (field.length < 2) {
      return "0" + int.toString();
    }
    else {
      return int.toString();
    }
  }
}
