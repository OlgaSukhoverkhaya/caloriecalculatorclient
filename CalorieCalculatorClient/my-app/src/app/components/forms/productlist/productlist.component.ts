import { Component, AfterViewInit, Inject, Output, EventEmitter } from '@angular/core';
import { DataService } from '../../../services/data.service';
import {MealDataService} from '../../../services/mealdata.service';
import { Product } from '../../../domain/product';
import { Meal} from '../../../domain/meal';
import { Observable } from 'rxjs/Observable';
import {MatTableDataSource} from '@angular/material';
import {MatDialog,MAT_DIALOG_DATA} from '@angular/material';
import {FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import 'rxjs/add/observable/of';
import { HomepageComponent } from '../../pages/homepage/homepage.component';
import { Subject } from 'rxjs';

@Component({
    selector: 'product-list',
    templateUrl: './productlist.component.html',
    styleUrls: ['./productlist.component.css'],
    providers: []
})
export class ProductListComponent implements AfterViewInit {
  
    displayedColumns=['productname','caloriecount','protein','fat','carbo','addmeal'];
    dataSource=new MatTableDataSource<Product>();
    error:any;
    mealToAdd:Meal=new Meal();
    public onlineUser = JSON.parse(localStorage.getItem('currentUser'));
    public userid = this.onlineUser.response.user_id;

    applyFilter(filterValue: string) {
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase();
        this.dataSource.filter = filterValue;
      }

    constructor(private dataService: DataService,private mealdataService:MealDataService,public dialog: MatDialog) { }

    ngAfterViewInit() {
       this.loadProducts();
       HomepageComponent.onTabchanged.subscribe(data=>this.loadProducts());
    }

    // получаем данные через сервис
    loadProducts() {
        this.dataService.getProducts().subscribe(
            data=>{this.dataSource.data=data},
            error => {this.error = error.message; console.log(error);})       
    }

    openDialog(product:Product) {
      const dialogRef = this.dialog.open(AddMealDialog, {
          maxHeight:'550px',
          maxWidth:'650px',
          data:{
            producttoadd:product,
            productid:product.productId
          }
        });
      
      dialogRef.afterClosed().subscribe(result => {
        console.log(`Dialog result: ${result}`);
        this.mealToAdd.productId=product.productId;
        this.mealToAdd.userId=this.userid;
        this.mealToAdd.weight=result;
        this.mealdataService.createMeal(this.mealToAdd)
            .subscribe((data: Meal) =>
            error => {this.error = error.message; console.log(error);});
      });
    }

}

@Component({
    selector: 'addmealdialog',
    templateUrl: 'addmealdialog.html',
  })
export class AddMealDialog {
  constructor(@Inject(MAT_DIALOG_DATA) public data: any) {}
  }

