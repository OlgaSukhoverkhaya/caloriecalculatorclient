
import { Progress } from "../../../domain/progress";
import { User } from "../../../domain/user";
import { OnInit, Component } from "@angular/core";
import { UserService } from "../../../services/user.service";



@Component({
  selector: 'app-vipprogress',
  templateUrl: './vipprogress.component.html',
  styleUrls: ['./vipprogress.component.css']
})
export class VipprogressComponent implements OnInit {
 user:User;
 progress:Progress[];
 public onlineUser = JSON.parse(localStorage.getItem('currentUser'));
 public userid = this.onlineUser.response.user_id;
 constructor(private userdataService:UserService) { }

  ngOnInit() {
  
    this.loadProgress();
    
  }
loadProgress()
{
    this.userdataService.getVipUserByUserId(this.userid)
    .subscribe(res=>{this.progress=res.progress});
}
}
