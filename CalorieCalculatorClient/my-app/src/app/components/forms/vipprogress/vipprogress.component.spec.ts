import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VipprogressComponent } from './vipprogress.component';

describe('VipprogressComponent', () => {
  let component: VipprogressComponent;
  let fixture: ComponentFixture<VipprogressComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VipprogressComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VipprogressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
