import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Product } from '../../../domain/product';
import { DataService } from '../../../services/data.service';
import {FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {EmptyObservable} from 'rxjs/observable/EmptyObservable';
import { ProductListComponent } from '../productlist/productlist.component';


@Component({
  selector: 'app-addproduct',
  templateUrl: './addproduct.component.html',
  styleUrls: ['./addproduct.component.css'],
  providers: []
})
export class AddproductComponent {

  product: Product = new Product();
  message:string;
  exists:boolean=false;
  error:any;
  products:Product[];

  constructor(private dataService: DataService) { }
 
  save(product:Product) {
    this.dataService.getProductByName(product.productName)
    .subscribe((data:Product)=>{
    if(data==null)
   {
        this.dataService.createProduct(product)
            .subscribe((data: Product) =>
            {this.message="Product "+product.productName+" is added!"},
            error => {this.error = error.message; console.log(error);});
    } 
    else{this.message="Product "+product.productName+" already exists!"}
  });
}

}

