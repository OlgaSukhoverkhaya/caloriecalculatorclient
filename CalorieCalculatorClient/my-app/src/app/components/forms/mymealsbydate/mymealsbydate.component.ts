import { Component, OnInit } from '@angular/core';
import { MealDataService } from '../../../services/mealdata.service';
import { Meal } from '../../../domain/meal';
import { Product } from '../../../domain/product';
import { User } from '../../../domain/user';
import { UserService } from '../../../services/user.service';
import { Observable } from 'rxjs/Observable';
import { HomepageComponent } from '../../pages/homepage/homepage.component';
import { AddMealDialog } from '../productlist/productlist.component';

@Component({
  selector: 'app-mymealsbydate',
  templateUrl: './mymealsbydate.component.html',
  styleUrls: ['./mymealsbydate.component.css']
})
export class MymealsbydateComponent implements OnInit {

  myMealsByDate: Meal[] = [];
  product: Product = new Product();
  error: any;
  user:User;
  public today = new Date();
  public onlineUser = JSON.parse(localStorage.getItem('currentUser'));
  public userid = this.onlineUser.response.user_id;

  public totalccal:number=0;

   constructor(private mealdataService: MealDataService,private userdataService:UserService) { }

 
  ngOnInit() {
    var formatted=this.today.toLocaleDateString();
    this.loadMealsByDate(formatted);
    HomepageComponent.onTabchanged.subscribe(data=>this.loadMealsByDate(formatted));
  }

  loadMealsByDate(date: string) {
    this.mealdataService.getMealsByDate(date, this.userid)
      .subscribe((data: Meal[]) => { this.myMealsByDate = data },
      error => { this.error = error.message; console.log(error); });
  }
  
}
