import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MymealsbydateComponent } from './mymealsbydate.component';

describe('MymealsbydateComponent', () => {
  let component: MymealsbydateComponent;
  let fixture: ComponentFixture<MymealsbydateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MymealsbydateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MymealsbydateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
