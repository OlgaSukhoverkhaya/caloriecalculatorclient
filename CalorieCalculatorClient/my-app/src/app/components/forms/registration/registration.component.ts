import { User } from '../../../domain/user';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { UserRole } from '../../../domain/userrole';
import { RegistrationService } from '../../../services/registration.service';
import { UserService } from '../../../services/user.service';
import { error } from 'util';
import { AuthenticationService } from '../../../services/autentication.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent {
  checked = false;
  model: any = {};
  confirmpassword: string = '';
  notconfirmed: boolean = false;
  error:string='';
  public result: boolean;
  
  constructor(private registrationService: RegistrationService, private userService: UserService,private router: Router,
    private authenticationService: AuthenticationService) { }


  @Output() onRegisterChanged = new EventEmitter<boolean>();
  change(tologin: any) {
    this.onRegisterChanged.emit(tologin);
  }

  registration() {
    if (this.model.password != this.confirmpassword) {
      this.notconfirmed = true;
    }
    else {
      this.addUser();
      this.notconfirmed = false; 
    
    }
  }


  addUser(){
    let newUser: User = new User();
    newUser.username = this.model.username;
    newUser.password = this.model.password;
   
    this.registrationService.createUser(newUser)
      .subscribe((res: any) => {},
      error=> {this.error = 'This username aready exists!'}
      ,
      ()=>{
        this.addUserRole();
          this.authenticationService.login(this.model.username,this.model.password)
          .subscribe(result=>
          {
            if(result==true)
            {
              this.router.navigate(['/home']); // login successful
            }
        })
      
     
    });
//  return result;
  };

  public newRole: UserRole=new UserRole();

addUserRole() {
     
     this.userService.getUserByUsername(this.model.username)
     .subscribe((result:User)=>{console.log(result.username);
      if (this.checked) 
     {
       this.newRole.roleid = 2;
      }
      else 
      {
        this.newRole.roleid = 1;
      }
      this.newRole.userid=result.userId
       this.registrationService.addUserRole(this.newRole)
       .subscribe()
     });
}
}
